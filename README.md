# Fireboy and Watergirl Co-op Platformer Game

A 2D cooperative platformer game inspired by the classic **"Fireboy and Watergirl"** series, built using Unity and C#. This project emphasizes engaging cooperative gameplay, challenging puzzles, and smooth character interactions.

## Features

- **Core Gameplay Mechanics**:  
  Designed and implemented character movement and physics-based interactions for a seamless gameplay experience.
  
- **Cooperative Gameplay**:  
  Players control two unique characters, Fireboy and Watergirl, requiring collaboration to solve puzzles and complete levels.
  
- **Physics Interactions**:  
  Integrated realistic physics-based interactions such as jumping, sliding, and object manipulation to navigate through levels.

- **Challenging Puzzles**:  
  Levels include intricate puzzles that leverage character-specific abilities, promoting teamwork and strategic planning.

## Technologies Used

- **Unity**: Game engine used for creating the 2D environment and handling game logic.  
- **C#**: Programming language used for scripting game mechanics and interactions.  
- **GitLab**: Version control and project collaboration platform.

## Getting Started

### Prerequisites
- Unity Editor (version 2021.3 or later recommended)
- Git (for cloning the repository)
