using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    private bool isdead;
    public ManageLevel GameManager;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if ((collision.gameObject.CompareTag("water") || collision.gameObject.CompareTag("goo")) && !isdead)
        {
            isdead = true;
            GameManager.GameOver();
            Destroy(gameObject);
            Debug.Log("Dead");
        }
    }
}
