using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class water_diamonds : MonoBehaviour
{
    private int diamond;
    public TextMeshProUGUI score;

    private void OnTriggerEnter2D(Collider2D other)
    {
        bool isfirediamond = other.gameObject.CompareTag("waterdiamond");
        if (isfirediamond)
        {
            diamond += 10;
            score.text = "Water : " + diamond.ToString();
            Debug.Log("Wdiamond");
            Destroy(other.gameObject);
        }

    }
}
