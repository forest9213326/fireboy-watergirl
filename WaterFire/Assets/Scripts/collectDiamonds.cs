using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class collectDiamonds : MonoBehaviour
{
    private int diamond;
    public TextMeshProUGUI score;

    private void OnTriggerEnter2D(Collider2D other)
    {
        bool isfirediamond = other.gameObject.CompareTag("firediamond");
        if (isfirediamond)
        {
            diamond += 10;
            score.text = "Fire : " + diamond.ToString();
            Debug.Log("diamond");
            Destroy(other.gameObject);
        }
        
    }
}
