﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MonoBehaviour
{
    [SerializeField] LeverController leverController;
    Vector3 targetPositionDown;
    Vector3 originalPosition;
    bool isMovingDown = false;
    bool hasMovedDown = false;
    bool isMovingUp = false;
    public float rotationThreshold = 45f; 
    public float reverseRotationThreshold = -45f; 

    void Start()
    {
        originalPosition = transform.position;
        targetPositionDown = new Vector3(transform.position.x, transform.position.y - 1.2f, transform.position.z);
    }

    void Update()
    {
        float leverRotation = leverController.GetLeverRotation();

        if (leverRotation > rotationThreshold && !hasMovedDown) 
        {
            isMovingDown = true;
            isMovingUp = false;
        }

        if (leverRotation < reverseRotationThreshold && hasMovedDown)
        {
            isMovingUp = true;
            isMovingDown = false;
        }

        if (isMovingDown)
        {
            MoveElevatorDown();
        }

        if (isMovingUp)
        {
            MoveElevatorUp();
        }
    }

    void MoveElevatorDown()
    {
        transform.position = Vector3.Lerp(transform.position, targetPositionDown, Time.deltaTime);

        if (Vector3.Distance(transform.position, targetPositionDown) < 0.005f)
        {
            transform.position = targetPositionDown;
            isMovingDown = false;
            hasMovedDown = true;
        }
    }

    void MoveElevatorUp()
    {
        transform.position = Vector3.Lerp(transform.position, originalPosition, Time.deltaTime);

        if (Vector3.Distance(transform.position, originalPosition) < 0.005f)
        {
            transform.position = originalPosition;
            isMovingUp = false;
            hasMovedDown = false;
        }
    }
}
