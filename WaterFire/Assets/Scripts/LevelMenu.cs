using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelMenu : MonoBehaviour
{
    
    
    public void Level1()
    {
        SceneManager.LoadScene("Level_1_Local");
        
    }
    
    public void Level2()
    {
        SceneManager.LoadScene("Level 2");
        
    }

    public void Home()
    {
        SceneManager.LoadScene("Homepage");
        
    }
}
