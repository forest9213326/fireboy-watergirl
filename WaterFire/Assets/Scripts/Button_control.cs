using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button_control : MonoBehaviour
{
    public Elevator1 elevator;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            elevator.Move_Down();
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            elevator.Move_Up();
        }
    }
}
