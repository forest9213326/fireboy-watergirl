using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator1 : MonoBehaviour
{
    private Vector2 originalPosition;
    private Vector2 targetPosition = new Vector2(4.1974f, -0.54f);
    private bool shouldMove = false;
    private Vector2 currentTarget;

    void Start()
    {
        originalPosition = transform.position;
        currentTarget = targetPosition;
    }

    void Update()
    {
        if (shouldMove)
        {
            Move_Lift(currentTarget);
        }
    }

    public void Move_Up()
    {
        currentTarget = originalPosition;
        shouldMove = true;
    }

    public void Move_Down()
    {
        currentTarget = targetPosition;
        shouldMove = true;
    }

    void Move_Lift(Vector2 target)
    {
        transform.position = Vector2.MoveTowards(transform.position, target, Time.deltaTime * 2);

        if ((Vector2)transform.position == target)
        {
            shouldMove = false;
        }
    }
}
