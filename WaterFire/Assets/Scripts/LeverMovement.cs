using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverController : MonoBehaviour
{
    public float strength = 35f; 
    public float minRotation = 0f; 
    public float maxRotation = 90f;
    private bool isPlayerColliding = false;
    private GameObject player;
    private Rigidbody2D rb;
    private float initialRotation;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        initialRotation = rb.rotation;
    }

    void Update()
    {
        if (isPlayerColliding && player != null)
        {
            float playerMovement = player.GetComponent<Rigidbody2D>().velocity.x; 
            float rotationAmount = -(playerMovement * strength * Time.deltaTime);
             float newRotation = rb.rotation + rotationAmount;

            newRotation = Mathf.Clamp(newRotation, initialRotation + minRotation, initialRotation + maxRotation);
            
            rb.MoveRotation(newRotation);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerColliding = true;
            player = collision.gameObject;
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerColliding = true;
            player = collision.gameObject;
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isPlayerColliding = false;
            player = null;
        }
    }

    public float GetLeverRotation()
    {
        return rb.rotation;
    }
}
