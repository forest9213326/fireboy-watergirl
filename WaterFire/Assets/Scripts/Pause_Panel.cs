using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause_Panel : MonoBehaviour
{
    [SerializeField] GameObject Pause_panel;
    public void pause()
    {
        Pause_panel.SetActive(true);
        Time.timeScale = 0;
    }
    public void retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1;
    }
    public void resume()
    {
        Pause_panel.SetActive(false);
        Time.timeScale = 1;
    }
    public void end()
    {
        SceneManager.LoadScene("MainMenu");
        Time.timeScale = 1;
    }

}
