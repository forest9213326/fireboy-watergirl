using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement_watergirl : MonoBehaviour
{
    public Rigidbody2D rigidPlayer;
    public float speed;
    public float jump;
    private float Move;
    private bool grounded;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Move = GetCustomHorizontalAxis();
        rigidPlayer.velocity = new Vector2(speed * Move, rigidPlayer.velocity.y);

        if (Input.GetButtonDown("JumpW") && grounded)
        {
            Jump();
        }
    }

    private void Jump()
    {
        rigidPlayer.AddForce(new Vector2(rigidPlayer.velocity.x, jump));
        grounded = false;
    }

    private float GetCustomHorizontalAxis()
    {
        float horizontal = 0f;

        if (Input.GetKey(KeyCode.A))
        {
            horizontal -= 1f;
        }
        if (Input.GetKey(KeyCode.D))
        {
            horizontal += 1f;
        }

        return horizontal;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "block" || collision.gameObject.tag == "water")
            grounded = true;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "block" || collision.gameObject.tag == "water")
        {
            grounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground" || collision.gameObject.tag == "block" || collision.gameObject.tag == "water")
        {
            grounded = false;
        }
    }
}
/*public Transform feet;
public LayerMask groundLayers;

public bool isGrounded()
{
    Collider2D groundCheck = Physics2D.OverlapCircle(feet.position, 0.5f, groundLayers);
    if (groundCheck != null)
        return true;
    return false;
}*/