using UnityEngine;
using UnityEngine.SceneManagement;

public class RedDeathScript : MonoBehaviour
{
    // Tags for BlueWater and GreenWater objects
    public string blueWaterTag = "BlueWater";
    public string greenWaterTag = "GreenWater";

    // Game Over UI
    public GameObject gameOverScreen;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(blueWaterTag) || other.CompareTag(greenWaterTag))
        {
            // Show the Game Over screen
            gameOverScreen.SetActive(true);

            // Stop the game
            Time.timeScale = 0;
        }
    }

    public void RestartGame()
    {
        // Reset the time scale
        Time.timeScale = 1;

        // Reload the current scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
