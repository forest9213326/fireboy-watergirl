using UnityEngine;

public class IgnoreCollision : MonoBehaviour
{
    public GameObject watergirl; // Reference to the Watergirl GameObject

    private Collider2D fireboyCollider;
    private Collider2D watergirlCollider;

    void Start()
    {
        // Get the Collider2D components from both characters
        fireboyCollider = GetComponent<Collider2D>();
        watergirlCollider = watergirl.GetComponent<Collider2D>();

        // Ignore collisions between Fireboy and Watergirl
        if (fireboyCollider != null && watergirlCollider != null)
        {
            Physics2D.IgnoreCollision(fireboyCollider, watergirlCollider);
        }
        else
        {
            Debug.LogError("Colliders not found on Fireboy or Watergirl.");
        }
    }
}
